// The vessel for my soul, according to brixen, is right here.
// I am looking to get into the C/++ part of rubinius, but it's difficult.
// The code is well done... It's well modularized, of course...
// But the C is intimidating, to say the least. People pour their lives into
// this, and for me, some kid, to come along and tap into it, it's not easy.

// So when I return in a month (7/17/08), I intend on getting into the C.
// Although maybe by then, heh, the C++ branch will be merged.
// I will grab a C++ book, a bag of M&Ms, and I'll start doc'ing it up.
// Because I figure that if I doc the code, I'll understand it better and
// learn a bit of C++ in the process.

// Let's add some content to this file, shall we?

// Bug with GC:
  // GC doesn't free up all objects
    // see running specs on 64 bit platform
    // check with brixen on it

// Bug with specs:
  // Check to make sure all files opened are closed
    // use `lsof`
  // temp files are made with "temp()"

// Half meditation, half dragon, half mortal, half demon. Age unknown, looks 22.
// My work here is done.